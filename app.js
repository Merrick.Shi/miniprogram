//app.js
import * as store from './utils/store.js';
import {
  phone_login,
  send_code,
  auth,
  auth_public,
  wechat_login,
} from './utils/api.js';

App({
  globalData: {
    userInfo: '',
    userid: 0,
    barHeight: 0,
    windowHeight: 0,
    code: '',
    ungetInfo: false,
    // shop_id: 182,
    shop_id: 182,
    imgRoot: 'https://xcx.xiqi-art.com/images/',
    appId: 'wx7f80beaa9876f2f6'
  },

  onLaunch: function() {
    let that = this
    wx.getSystemInfo({
      success: function(res) {
        that.globalData.barHeight = res.statusBarHeight,
        that.globalData.windowHeight = res.windowHeight
      }
    })


    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        that.globalData.code = res.code

        // 获取用户信息
        wx.getSetting({
          success: (res) => {
            if (res.authSetting['scope.userInfo']) {
              // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框  
              wx.getUserInfo({
                success: res => {
                  store.set('me', res.userInfo)
                  // 可以将 res 发送给后台解码出 unionId
                  that.globalData.userInfo = res.userInfo
                  wechat_login({
                    code: that.globalData.code,
                    nickName: res.userInfo.nickName,
                    avatarUrl: res.userInfo.avatarUrl
                  }).then((response) => {
                    if (response.status == 200) {
                      that.globalData.userid = response.data.userId
                      store.set('user_id', response.data.userId)
                    }
                  }).then(function() {
                    if (that.userInfoReadyCallback) {
                      that.userInfoReadyCallback(that.globalData.userid, that.globalData.ungetInfo)
                    }
                  })
                  // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                  // 所以此处加入 callback 以防止这种情况
                }
              })
            } else {
              that.globalData.ungetInfo = true
              if (that.userInfoReadyCallback) {
                that.userInfoReadyCallback(that.globalData.userid, that.globalData.ungetInfo)
              }
            }
          },
          fail: (res) => {
            wx.authorize({
              scope: 'scope.userInfo',
              success() {
                that.onLaunch()
              }
            })
          }
        })
      }
    })

  }

})