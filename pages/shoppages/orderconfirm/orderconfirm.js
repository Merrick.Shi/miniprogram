// pages/shoppages/orderconfirm/orderconfirm.js
import {
    query_order,
    wechat_login
} from '../../../utils/api.js'
import {
    base
} from '../../../utils/configs/authConfig';
const app = getApp()
const moment = require('../../../utils/moment.js');

Page({

    /**
     * 页面的初始数据
     */
    data: {
        orderno: "",
        orderinfo: {},
        roomtypes: []
    },

    formSubmit(e) {
        wx.showLoading({
            title: '',
        });
        var code = ''     //传给服务器以获得openId
        var timestamp = ''   //时间戳
        var nonceStr = ''   //随机字符串，后台返回
        var prepayId = ''    //预支付id，后台返回
        var paySign = ''     //加密字符串
        var that = this
        //获取用户登录状态
        wx.login({
            success: res => {
                wechat_login({
                    code: res.code
                }).then((res) => {
                    if (res.status == 200) {
                        wx.request({
                            url: base + '/payorder',
                            data: {
                                openId: res.data.openid,
                                userId: res.data.userId,
                                orderNo: that.data.orderno
                            },
                            method: 'POST',
                            success: function (res) {
                                if (res.data.result == true) {
                                    nonceStr = res.data.nonceStr
                                    prepayId = res.data.prepayId
                                    timestamp = res.data.timeStamp
                                    paySign = res.data.paySign
                                    // 发起微信支付
                                    wx.requestPayment({
                                        timeStamp: timestamp,
                                        nonceStr: nonceStr,
                                        package: 'prepay_id=' + prepayId,
                                        signType: 'MD5',
                                        paySign: paySign,
                                        success: function (res) {
                                            wx.request({
                                                url: base + '/orderpaid',
                                                method: 'POST',
                                                data: {
                                                    userId: app.globalData.userid,
                                                    orderNo: that.data.orderno
                                                },
                                                success: function (res) {
                                                    wx.hideLoading();
                                                    wx.navigateTo({
                                                        url: '../../mine/orderlist/orderlist?userId='+app.globalData.userid
                                                    })
                                                },
                                            })
                                        },
                                        fail: function (res) {
                                            wx.showLoading({
                                                title: res.errMsg,
                                            })
                                            setTimeout(function () {
                                                wx.hideLoading()
                                                that.setData({
                                                    disable: false
                                                })
                                            }, 2000)
                                        }
                                    })
                                } else {
                                    
                                }
                            }
                        })
                    }
                })
            }
        });
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        var that = this

        if (options.orderno != '') {
            that.data.orderno = options.orderno;
            query_order({
                userId: app.globalData.userid,
                orderNo: that.data.orderno
            }).then((res) => {
                if (res != '' && res.status == 200 && res.data != null) {
                    res.data.order.arriveDate = moment.utc(new Date(res.data.order.arriveDate)).format('YYYY-M-D');

                    var roomTypesMap = {};
                    for(var i =0;i<res.data.orderRooms.length;i++){
                        if(roomTypesMap[res.data.orderRooms[i].roomTypeName] == undefined){
                            roomTypesMap[res.data.orderRooms[i].roomTypeName] = {};
                            roomTypesMap[res.data.orderRooms[i].roomTypeName]['count'] = 1
                            roomTypesMap[res.data.orderRooms[i].roomTypeName]['amount'] = res.data.orderRooms[i].amount;
                        }else{
                            roomTypesMap[res.data.orderRooms[i].roomTypeName].count += 1; 
                        }   
                    }

                    var roomTypes = [];
                    
                    for(var key in roomTypesMap){
                        var item = {};
                        item.roomTypeName = key;
                        item.roomCount = roomTypesMap[key]['count'];
                        item.roomAmount = roomTypesMap[key]['amount'];
                        roomTypes.push(item);
                    }
                    that.setData({
                        orderinfo: res.data,
                        roomtypes: roomTypes
                    });
                } else {
                    wx.showLoading({
                        title: '参数错误',
                    })
                    setTimeout(function () {
                        wx.hideLoading()
                        that.setData({
                            disable: false
                        })
                    }, 2000)
                }
            })
        } else {
            wx.showLoading({
                title: '参数错误',
            })
            setTimeout(function () {
                wx.hideLoading()
                that.setData({
                    disable: false
                })
            }, 2000)
        }

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})