// pages/shoppages/mall/mall.js
import {
  course_query
} from '../../../utils/api.js';
import * as store from '../../../utils/store.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    constants: [],
    toView: null,
    currentLeftSelect: null,
    eachRightItemToTop: [],
    leftToTop: 0,
    courseId:""
  },

  getEachRightItemToTop: function() {
    var that = this
    var obj = {};
    var totop = 0;
    const RIGHT_BAR_HEIGHT = 30;
    const RIGHT_ITEM_HEIGHT = 160;
    obj[that.data.constants[0].groupId] = totop
    for (let i = 1; i < (that.data.constants.length + 1); i++) {
      totop += (RIGHT_BAR_HEIGHT + that.data.constants[i - 1].courses.length * RIGHT_ITEM_HEIGHT)
      obj[that.data.constants[i] ? that.data.constants[i].groupId : 'last'] = totop
    }
    return obj
  },

  rightScroll: function(e) {
    const LEFT_ITEM_HEIGHT = 50
    for (let i = 0; i < this.data.constants.length; i++) {
      let left = this.data.eachRightItemToTop[this.data.constants[i].groupId]
      let right = this.data.eachRightItemToTop[this.data.constants[i + 1] ? this.data.constants[i + 1].groupId : 'last']
      if (e.detail.scrollTop < right && e.detail.scrollTop >= left) {
        this.setData({
          currentLeftSelect: this.data.constants[i].groupId,
          leftToTop: LEFT_ITEM_HEIGHT * i
        })
      }
    }
  },

  jumpToSick: function(e) { 
    this.setData({
      toView: e.target.id || e.target.dataset.id,
      currentLeftSelect: e.currentTarget.dataset.id
    })
  },



  single_order(e) {
    var that = this
    
    wx.setStorage({
      key: 'singleorderinfo',
      data: [{
        course_id: e.currentTarget.dataset.course_id,
        course_name: e.currentTarget.dataset.course_name,
        price_person: e.currentTarget.dataset.price_person,
        price_org: e.currentTarget.dataset.price_org,
        group_name: e.currentTarget.dataset.group_name,
        count: 1
      }],
    })

    wx.navigateTo({
      url: '/pages/shoppages/singleorderinfo/singleorderinfo',
    })
  },

  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    course_query({
    }).then((res) => {
      this.data.constants = res.data
      this.setData({
        constants: res.data,
        currentLeftSelect: res.data[0].groupId,
        eachRightItemToTop: this.getEachRightItemToTop(),
        ungetInfo: app.globalData.ungetInfo
      })
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})