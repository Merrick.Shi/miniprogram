// pages/shoppages/singleorderinfo/singleorderinfo.js
import {
    place_order,
    hotel_room_query,
    wechat_login
} from '../../../utils/api.js'
import * as store from '../../../utils/store.js'
const app = getApp()

var dateTimePicker = require('../../../utils/dateTimePicker.js');


Page({

    /**
     * 页面的初始数据
     */
    data: {
        array: ['男', '女'],
        objectArray: [{
            id: 0,
            name: '男'
        },
        {
            id: 1,
            name: '女'
        }
        ],
        totalMoney: 0,
        shopa: "",
        shop_phone: '',
        date_info: '',
        selected_rooms: [],
        room_type_array: [],
        day_count: 1,
        isPickerRender: false,
        isPickerShow: false,
        isRoomSelectShow: false,
        isDateInfoShow: false,
        startTime: "2019-01-01",
        endTime: "2019-12-01",
        pickerConfig: {
            endDate: true,
            column: "date",
            dateLimit: true,
            initStartTime: (new Date()).toLocaleDateString(),
            initEndTime: (new Date()).toLocaleDateString(),
            limitStartTime: "2015-05-06",
            limitEndTime: "2055-05-06",
            yearStart: (new Date()).getFullYear(),
            yearEnd: (new Date()).getFullYear() + 1,
        }
    },

    totalPrice: function () {
        var that = this;
        var total_price = 0;
        for (var i = 0; i < that.data.selected_course.length; i++) {
            total_price += parseFloat(that.data.selected_course[i].price_person);
            // total_price += that.data.selected_course[i].count * parseFloat(that.data.selected_course[i].price_person)
        }
        that.setData({
            totalPrice: total_price.toFixed(2),
            totalMoney: total_price.toFixed(2)
        });
    },

    addRoom: function (e) {
        var that = this;
        var index = e.currentTarget.dataset.index;
        var selectRoom = that.data.room_type_array[index];

        var room = {};
        room.id = selectRoom.id;
        room.roomId = selectRoom.roomId;
        room.roomTypeName = selectRoom.roomTypeName;
        room.amount = selectRoom.amount;
        room.guestName = '';

        that.data.selected_rooms.push(room);
        var amount = parseFloat(that.data.selected_course[0].price_person);
        for (var i = 0; i < that.data.selected_rooms.length; i++) {
            amount += that.data.day_count * that.data.selected_rooms[i].amount;
        }

        that.setData({
            rooms: that.data.selected_rooms,
            totalMoney: amount,
            isRoomSelectShow: true
        });
    },

    fillName: function (e) {
        var that = this;
        var name = e.detail.value;
        var index = e.currentTarget.dataset.index;
        that.data.selected_rooms[index].guestName = name;
        that.setData({
            rooms: that.data.selected_rooms
        });
    },

    removeRoom: function (e) {
        var that = this;
        var index = e.currentTarget.dataset.index;
        var selectRoom = that.data.selected_rooms[index];
        var amount = parseFloat(that.data.totalMoney) - parseFloat(selectRoom.amount) * that.data.day_count;

        that.data.selected_rooms.splice(index, 1);
        that.setData({
            rooms: that.data.selected_rooms,
            totalMoney: amount
        });

        if (that.data.selected_rooms.length == 0) {
            that.setData({
                isRoomSelectShow: false
            });
        }
    },

    checkDate: function (dateStr) {
        var result = true;

        var dateStrTimestamp = Date.parse(new Date(dateStr));
        dateStrTimestamp = dateStrTimestamp / 1000;

        var timestamp = Date.parse(new Date());
        timestamp = timestamp / 1000;

        if (dateStrTimestamp > timestamp) {
            result = false;
        }
        return result;
    },

    //付款
    formSubmit(e) {
        var that = this;
        if (!app.globalData.userid.length < 36) {
            if (!store.get('user_id') && store.get('user_id').length == 36) {
                app.globalData.userid = store.get('user_id');
            } else {
                wx.login({
                    success: res => {
                        // 发送 res.code 到后台换取 openId, sessionKey, unionId
                        app.globalData.code = res.code
                        store.set('me', e.detail.userInfo)
                        // 可以将 res 发送给后台解码出 unionId
                        app.globalData.userInfo = e.detail.userInfo
                        wechat_login({
                            code: app.globalData.code
                        }).then((response) => {
                            if (response.status == 200) {
                                app.globalData.userid = response.data.userId
                                store.set('openid', response.data.openId)
                                store.set('user_id', response.data.userId)
                                var order_course = []
                                var flag = true

                                wx.showLoading({
                                    title: '',
                                });

                                var params = e.detail.value;

                                if (params.contact_name == '') {
                                    wx.showLoading({
                                        title: '请填写姓名',
                                    })
                                    setTimeout(function () {
                                        wx.hideLoading()
                                    }, 1000)
                                } else if (params.contact_phone == '') {
                                    wx.showLoading({
                                        title: '请填写手机号',
                                    })
                                    setTimeout(function () {
                                        wx.hideLoading()
                                    }, 1000)
                                } else if (!(/^[1][3,4,5,7,8][0-9]{9}$/.test(params.contact_phone))) {
                                    wx.showLoading({
                                        title: '手机号不正确',
                                    })
                                    setTimeout(function () {
                                        wx.hideLoading()
                                    }, 1000)
                                } else if (that.data.selected_rooms.length > 0) {
                                    var guestNameEmpty = false;
                                    for (var i = 0; i < that.data.selected_rooms.length; i++) {
                                        if (!guestNameEmpty && (that.data.selected_rooms[i].guestName == '' || that.data.selected_rooms[i].guestName == undefined)) {
                                            guestNameEmpty = true;
                                            break;
                                        }
                                    }
                                    if (guestNameEmpty) {
                                        wx.showLoading({
                                            title: '请填入住人姓名',
                                        })
                                        setTimeout(function () {
                                            wx.hideLoading()
                                        }, 1000)
                                    } else if (that.data.day_count == 0) {
                                        wx.showLoading({
                                            title: '请填写入住时间',
                                        })
                                        setTimeout(function () {
                                            wx.hideLoading()
                                        }, 1000)
                                    } else {
                                        flag = false;
                                    }
                                } else {
                                    flag = false
                                }
                                if (flag == false) {
                                    for (var i = 0; i < this.data.selected_course.length; i++) {
                                        var item = this.data.selected_course[i]

                                        order_course.push({
                                            "courseCount": item.count,
                                            "payAmount": parseFloat(item.price_person),
                                            "courseId": item.course_id,
                                            "courseName": item.course_name
                                        })
                                    }
                                    that.setData({
                                        disable: true,
                                    })

                                    place_order({
                                        userId: app.globalData.userid,
                                        courseId: order_course[0].courseId,
                                        courseName: order_course[0].courseName,
                                        orgName: params.org_name,
                                        attendanceCount: params.attendance_count || 1,
                                        contactName: params.contact_name,
                                        contactPhone: params.contact_phone,
                                        courseCount: 1,
                                        arriveDate: that.data.startTime,
                                        dayCount: params.day_count == undefined ? 0 : params.day_count,
                                        courseAmount: order_course[0].payAmount,
                                        amount: that.data.totalMoney,
                                        rooms: that.data.rooms,
                                        orderComment: params.comment || ''
                                    }).then((res) => {
                                        wx.hideLoading();
                                        if (res != '' && res.status == 200 && res.data != '') {
                                            wx.navigateTo({
                                                url: '/pages/shoppages/orderconfirm/orderconfirm?orderno=' + res.data,
                                            })
                                        } else {
                                            wx.showLoading({
                                                title: '参数错误',
                                            })
                                            setTimeout(function () {
                                                wx.hideLoading()
                                                that.setData({
                                                    disable: false
                                                })
                                            }, 2000)
                                        }
                                    })
                                }
                            }
                        })
                    }
                })
            }
        }

    },

    //选择日期
    bindDateChange: function (e) {
        this.setData({
            date: e.detail.value
        })
    },



    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        var that = this
        store.set('coupon', '')

        hotel_room_query().then((res) => {

            that.setData({
                room_type_array: res.data,
                day_count: 0
            })
        }),


            wx.getStorage({
                key: 'singleorderinfo',
                success: function (res) {
                    that.setData({
                        selected_course: res.data,
                    })
                    that.totalPrice()
                },
            })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        let that = this
        wx.getStorage({
            key: 'editaddress',
            success: function (res) {
                if (res != '') {
                    that.setData({
                        editaddress: res.data
                    })
                }
            },
        })
        that.data.coupon = store.get('coupon')
        if (that.data.coupon != '') {
            if (that.data.coupon.cash_type == 1) {
                let total = that.data.totalPrice * that.data.coupon.cash_money / 10
                that.setData({
                    discount: that.data.totalPrice * (10 - that.data.coupon.cash_money) / 10,
                    totalMoney: total.toFixed(2),
                    coupon_id: that.data.coupon.cash_id
                })
            } else if (that.data.coupon.cash_type == 0) {
                let total = that.data.totalPrice - that.data.coupon.cash_money
                that.setData({
                    discount: that.data.coupon.cash_money,
                    totalMoney: total.toFixed(2),
                    coupon_id: that.data.coupon.cash_id
                })
            }
        }
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    pickerShow: function () {
        this.setData({
            isPickerShow: true,
            isPickerRender: true,
            chartHide: true
        });
    },
    pickerHide: function () {
        this.setData({
            isPickerShow: false,
            chartHide: false
        });
    },

    setPickerTime: function (val) {
        var that = this;
        let data = val.detail;

        var day = that.GetDateDiff(data.startTime, data.endTime);
        if (day < 1) {
            wx.showLoading({
                title: '请填写正确的时间',
            })
            setTimeout(function () {
                wx.hideLoading()
            }, 1000)
        }
        var amount = parseFloat(that.data.selected_course[0].price_person);
        for (var i = 0; i < that.data.selected_rooms.length; i++) {
            amount += day * that.data.selected_rooms[i].amount;
        }

        let dateInfo = new Date(Date.parse(data.startTime)).toLocaleDateString() + '-' + new Date(Date.parse(data.endTime)).toLocaleDateString();

        this.setData({
            isDateInfoShow: true,
            startTime: data.startTime,
            endTime: data.endTime,
            day_count: day,
            totalMoney: amount,
            date_info: dateInfo
        });
    },

    GetDateDiff: function (startDate, endDate) {
        var startTime = new Date(Date.parse(startDate.replace(/-/g, "/"))).getTime();
        var endTime = new Date(Date.parse(endDate.replace(/-/g, "/"))).getTime();
        var dates = Math.abs((startTime - endTime)) / (1000 * 60 * 60 * 24);
        return dates;
    }
})