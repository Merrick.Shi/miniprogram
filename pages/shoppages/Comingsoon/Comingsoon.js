// pages/shoppages/Comingsoon/Comingsoon.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page: '',
    images: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var page = options.page;
    var imageSource = [];
    switch (page) {
      case 'masterTalk':
        imageSource = ['https://xcx.xiqi-art.com/images/mastertalk1.png', 'https://xcx.xiqi-art.com/images/mastertalk2.png', 
        'https://xcx.xiqi-art.com/images/mastertalk3.png', 'https://xcx.xiqi-art.com/images/mastertalk4.png', 
        'https://xcx.xiqi-art.com/images/mastertalk5.png'];
        break;
      case 'onlineTraining':
        imageSource = ['https://xcx.xiqi-art.com/images/onlinetraining1.png', 'https://xcx.xiqi-art.com/images/onlinetraining2.png', 
        'https://xcx.xiqi-art.com/images/onlinetraining3.png'];
        break;
      case 'xiqiShow':
        imageSource = ['https://xcx.xiqi-art.com/images/xiqishow1.png', 'https://xcx.xiqi-art.com/images/xiqishow2.png', 
        'https://xcx.xiqi-art.com/images/xiqishow3.png','https://xcx.xiqi-art.com/images/xiqishow4.png', 
        'https://xcx.xiqi-art.com/images/xiqishow5.png', 'https://xcx.xiqi-art.com/images/xiqishow6.png',
          'https://xcx.xiqi-art.com/images/xiqishow7.png', 'https://xcx.xiqi-art.com/images/xiqishow8.png', 
          'https://xcx.xiqi-art.com/images/xiqishow9.png','https://xcx.xiqi-art.com/images/xiqishow10.png'];
        break;
      case 'master1':
        imageSource = ['https://xcx.xiqi-art.com/images/masterinfo1.jpg'];
        break;
      case 'master2':
        imageSource = ['https://xcx.xiqi-art.com/images/masterinfo2.jpg'];
        break;
      case 'master3':
        imageSource = ['https://xcx.xiqi-art.com/images/masterinfo3.jpg'];
        break;
      case 'master4':
        imageSource = ['https://xcx.xiqi-art.com/images/masterinfo4.jpg'];
        break;
      case 'master5':
        imageSource = ['https://xcx.xiqi-art.com/images/masterinfo5.jpg'];
        break;
    }


    that.setData({
      images: imageSource
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})