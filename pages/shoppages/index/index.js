// pages/shoppages/index/index.js

import {
  shop_info,
  get_cash,
  show_commend,
  auth_public,
  wechat_login,
} from '../../../utils/api.js';
import qqVideo from "../../../utils/qqvideo.js"
import * as store from '../../../utils/store.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    imgRoot: app.globalData.imgRoot,
    isAuto: false,
    showPosterTop: true,
    showPosterLeft: true,
    showPosterRight: true,
    showPosterBottom: true,
    showVedioLeft: false,
    showVedioRight: false,
    showVedioBottom: false,
    videolist: ['f0854enbpyv', 'm0843vzrse6', 'v0843em2hgz', 'h0854rfya3a'],
    hasUserInfo: false,
    videoUrl0: '',
    videoUrl1: '',
    videoUrl2: '',
    videoUrl3: '',
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    nav: [

    ]
  },

  intoUrl: function (e) {

    let url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: '/pages/ss/ss?url=' + url,

    })
  },

  // 搜索
  to_search_goods() {
    wx.navigateTo({
      url: '/pages/shoppages/search/search',
    })
  },

  // 点击poster播放
  playVideoTop() {
    var that = this;
    qqVideo.getVideoes(that.data.videolist[0]).then(function (response) {
      that.setData({
        videoUrl0: response[0],
      });
    });
    let vediocon = wx.createVideoContext("myvedioTop", this)
    vediocon.play()
    this.setData({
      showPosterTop: false
    })
  },

  playVideoLeft() {
    var that = this;
    qqVideo.getVideoes(that.data.videolist[1]).then(function (response) {
      that.setData({
        videoUrl1: response[0],
      });
    });
    let vediocon = wx.createVideoContext("myvedioLeft", this)
    vediocon.play()
    this.setData({
      showPosterLeft: false
    })
  },

  playVideoRight() {
    var that = this;
    qqVideo.getVideoes(that.data.videolist[2]).then(function (response) {
      that.setData({
        videoUrl2: response[0],
      });
    });
    let vediocon = wx.createVideoContext("myvedioRight", this)
    vediocon.play()
    this.setData({
      showPosterRight: false
    })
  },

  playVideoBottom() {
    var that = this;
    qqVideo.getVideoes(that.data.videolist[3]).then(function (response) {
      that.setData({
        videoUrl3: response[0],
      });
    });
    let vediocon = wx.createVideoContext("myvedioBottom", this)
    vediocon.play()
    this.setData({
      showPosterBottom: false
    })
  },


  // 跳转敬请期待页面

  Comingsoon(e) {
    var masters = ['master1', 'master2', 'master3', 'master4', 'master5']
    var page = e.currentTarget.dataset.id;

    if (masters.indexOf(page) > -1) {
      wx.navigateTo({
        url: '/pages/shoppages/Comingsoon/Comingsoon?page=' + page,
      })
    } else {
      wx.navigateTo({
        url: '/pages/shoppages/Comingsoon/Comingsoon2?page=' + page,
      })
    }

  },

  // 授权登录
  bindGetUserInfo: function (e) {
    var that = this
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        app.globalData.code = res.code
        store.set('me', e.detail.userInfo)
        // 可以将 res 发送给后台解码出 unionId
        app.globalData.userInfo = e.detail.userInfo
        wechat_login({
          code: app.globalData.code
        }).then((response) => {
          if (response.status == 200) {
            app.globalData.userid = response.data.userId
            store.set('openid', response.data.openId)
            store.set('user_id', response.data.userId)
            that.setData({
              showLoad: false,
              avatar: e.detail.userInfo.avatarUrl,
              name: e.detail.userInfo.nickName
            })
          }
        })
      }
    })
  },


  bindcation() {
    wx.navigateTo({
      url: '/pages/shoppages/mall/mall',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (app.globalData.userid == '0') {
      wx.login({
        success: res => {
          // 发送 res.code 到后台换取 openId, sessionKey, unionId
          app.globalData.code = res.code
          wechat_login({
            code: app.globalData.code
          }).then((response) => {
            if (response.status == 200) {
              app.globalData.userid = response.data.userId
              store.set('openid', response.data.openid)
              store.set('user_id', response.data.userId)
              that.setData({
                showLoad: false
              })
            }
          })
        }
      })
    }
    let that = this
    if (!store.get('user_id')) {
      that.setData({
        showLoad: true
      })
    }
    that.setData({
      imgUrls: [that.data.imgRoot + 'banner.png'],
      imgRoot: that.data.imgRoot
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})