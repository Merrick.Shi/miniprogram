// pages/mine/mine.js
import {
  get_user_info,
  wechat_login
} from '../../utils/api.js'
import * as store from '../../utils/store.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },

  bindGetUserInfo: function (e) {
    var that = this
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        app.globalData.code = res.code
        store.set('me', e.detail.userInfo)
        // 可以将 res 发送给后台解码出 unionId
        app.globalData.userInfo = e.detail.userInfo
        wechat_login({
          code: app.globalData.code
        }).then((response) => {
          if (response.status == 200) {
            app.globalData.userid = response.data.userId
            store.set('openid', response.data.openId)
            store.set('user_id', response.data.userId)
            that.setData({
              showLoad: false,
              avatar: e.detail.userInfo.avatarUrl,
              name: e.detail.userInfo.nickName
            })
          }
        })
      }
    })
  },

  user_info() {
    var that = this
    get_user_info({
      userId: app.globalData.userid
    }).then((res) => {
      if (res.status = 200 && res.data != null) {
        that.setData({
          avatar: res.data.avatarUrl,
          name: res.data.userName
        })
      }
    })
  },
  
  // 我的报名
  tonotreceive(e) {
    wx.navigateTo({
      url: '/pages/mine/orderlist/orderlist?bindid=' + e.currentTarget.dataset.bindid,
    })
  },
  
  //优惠券
  tocoupon() {
    wx.navigateTo({
      url: '/pages/mine/coupon/coupon',
    })
  },

  // 我的拼团
  tomygroup() {
    wx.navigateTo({
      url: '/pages/mine/groupbuy/groupbuy',
    })
  },

  commingsoon(){
    wx.navigateTo({
      url: '/pages/shoppages/Comingsoon/Comingsoon',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(app.globalData.userid == '0'){
      app.globalData.userid = store.get('user_id');
    }
    let that = this
    if (!store.get('user_id')) {
      that.setData({
        showLoad: true
      })
    }
    that.user_info()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})