import {
  paid_orders,
  finished_orders
} from '../../../utils/api.js'
import * as store from '../../../utils/store.js'
const app = getApp()

Page({
  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
    moreHandel: true,
    winHeight: 0,
    notpaylist: [],
    notreceivelist: [],
    order_num: 0,
    notpostlist: [],
  },


  swichNav(e) {
    var that = this
    if (that.data.currentTab == e.currentTarget.dataset.current) {
      return false;
    } else {
      that.data.currentTab = e.currentTarget.dataset.current
      that.show();
    }
  },

  /**
   * 待体验
   */

  notpost() {
    var that = this
    paid_orders({
      userId: app.globalData.userid
    }).then((res) => {
      if (res.status == 200) {
        var notpostlist_pre = res.data
        that.setData({
          winHeight: res.data.length * 400,
          order_num: res.data.length,
          notpostlist: notpostlist_pre,
          currentTab: 0
        })
      } else {
        that.setData({
          winHeight: res.data.length * 300,
          order_num: res.data.length,
          currentTab: 1
        })
      }
    })
  },

  /**
   * 已完成方法
   */
  finish() {
    var that = this
    finished_orders({
      userId: app.globalData.userid
    }).then((res) => {
      if (res.status == 200) {
        var finishlist_pre = res.data
        that.setData({
          finishlist: finishlist_pre,
          winHeight: res.data.length * 400,
          order_num: res.data.length,
          currentTab: 1
        })
      } else {
        that.setData({
          winHeight: res.data.length * 300,
          order_num: res.data.length,
          currentTab: 1
        })
      }
    })
  },

  show() {
    var that = this
    if (that.data.currentTab == 0) {
      that.notpost()
    } else if (that.data.currentTab == 1) {
      that.finish()
    }
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    that.setData({
      bar: app.globalData.barHeight,
      currentTab: 0
    })
    that.show()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // var that = this
    // that.show()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})